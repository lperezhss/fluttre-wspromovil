import json
import os
from datetime import datetime

import pymssql
import pypyodbc

basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

def set_log_file(log_str):
    log_file_name = "log_" + datetime.now().strftime("%Y_%m_%d") + ".log"
    log_file_path = os.path.join(basedir, "log")
    log_file = os.path.join(log_file_path, log_file_name)

    with open(log_file, "a") as log_file:
        date_log = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        log_file.write(date_log + ' -> ' + log_str + "\n")


def get_config():
    path_file = os.path.join(basedir, 'config')
    config_file = os.path.join(path_file, 'config.json')

    fd = open(config_file, "r")
    config_txt = fd.read()
    config_json = json.loads(config_txt)
    fd.close()

    return list(config_json.values())


def get_conn():
    server, database, username, password, port = get_config()
    # tcon = 'no'

    try:
        conn = pymssql.connect(
            server=server,
            port=port,
            user=username,
            password=password,
            database=database
        )
    except Exception as e:
        if 'DB-Li' in str(e):
            try:
                conn = pypyodbc.connect(
                    driver='{SQL Server}',
                    server='tcp:' + server + ',' + port,
                    uid=username,
                    pwd=password,
                    database=database
                )
            except Exception as e2:
                raise str(e2)
        else:
            raise str(e)

    return conn


def get_pm_response(status, record_id, message, json):
    return {
      "id": record_id,
      "status": status,
      "message": message,
      "json": json
    }


def get_local_url():
    return 'http://localhot:1559'
