select rtrim(ltrim(co_cli)) id,
	rtrim(ltrim(co_cli)) value,
	rtrim(ltrim(cli_des)) name,
	rtrim(ltrim(rif)) taxid,
	rtrim(ltrim(telefonos)) phone,
	convert(varchar(max),direc1) address,
	rtrim(ltrim(co_ven)) vendor,
	rtrim(ltrim(co_zon)) zone,
	rtrim(ltrim(email)) email,
	right(rtrim((select mm.co_precio from saTipoCliente mm where mm.tip_cli = saCliente.tip_cli)),1) tprice,
	case 
		when lunes = 1 then 'LUNES' 
		when martes = 1 then 'MARTES' 
		when miercoles = 1 then 'MIERCOLES' 
		when jueves = 1 then 'JUEVES' 
		when viernes = 1 then 'VIERNES' 
		when sabado = 1 then 'SABADO' 
		when domingo = 1 then 'DOMINGO' 
		else 'ND' 
	end + '|' + isnull(frecu_vist,'') route,
	(select max(cupon) from _pm_config) coupon
from saCliente 
where cli_des like '%%'
	and inactivo = 0
	and #PARAMETER1#
order by co_cli
