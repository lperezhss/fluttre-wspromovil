select rtrim(ltrim(co_lin)) id,
	rtrim(ltrim(co_lin)) value,
	rtrim(ltrim(lin_des)) name,
	(select max(cupon) from _pm_config) coupon
from saLineaArticulo
order by co_lin
