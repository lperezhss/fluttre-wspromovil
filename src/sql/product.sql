select rtrim(ltrim(AA.co_art)) id, 
	rtrim(ltrim(AA.co_art)) value, 
	rtrim(ltrim(AA.art_des)) name, 
	rtrim(ltrim(AA.co_lin)) category_id, 
	AA.tipo type, 
	AA.tipo_imp taxtype, 
	ISNULL(AA.ref,'') ref, 
	sum((case when BB.tipo = 'ACT' then BB.stock else 0 end) - (case when BB.tipo = 'COM' then BB.stock else 0 end)) stock, 
	convert(decimal(18,2),round(isnull((select top(1) CC.monto from saArtPrecio CC where CC.co_art = AA.co_art and CC.co_precio = '01' and CC.co_alma_calculado = '01' order by CC.desde desc),0)/1,2)) price1, 
	convert(decimal(18,2),round(isnull((select top(1) CC.monto from saArtPrecio CC where CC.co_art = AA.co_art and CC.co_precio = '02' and CC.co_alma_calculado = '01' order by CC.desde desc),0)/1,2)) price2, 
	convert(decimal(18,2),round(isnull((select top(1) CC.monto from saArtPrecio CC where CC.co_art = AA.co_art and CC.co_precio = '03' and CC.co_alma_calculado = '01' order by CC.desde desc),0)/1,2)) price3, 
	convert(decimal(18,2),round(isnull((select top(1) CC.monto from saArtPrecio CC where CC.co_art = AA.co_art and CC.co_precio = '04' and CC.co_alma_calculado = '01' order by CC.desde desc),0)/1,2)) price4, 
	convert(decimal(18,2),round(isnull((select top(1) CC.monto from saArtPrecio CC where CC.co_art = AA.co_art and CC.co_precio = '05' and CC.co_alma_calculado = '01' order by CC.desde desc),0)/1,2)) price5, 
    A.des_uni uom1,
	A.des_uni uom2,
	DD.equivalencia equivalent1, 
	DD.equivalencia equivalent2,
	ltrim(rtrim(IM.co_imag)) + '.' + LOWER(IM.co_tipo_imag) file_name,
	(select max(cupon) from _pm_config) coupon
from saArticulo AA 
	left outer join saDocumentoImagen IM on AA.rowguid = IM.rowguidDoc and IM.co_tipo_doc = 'ARTIC'
	left outer join saStockAlmacen BB on AA.co_art = BB.co_art, saArtUnidad DD, saUnidad A 
where (AA.art_des like '%#PARAMETER1#%' or AA.co_art like '%#PARAMETER1#%' or AA.ref like '%#PARAMETER1#%') 
	and AA.anulado = '0' 
	and AA.co_art = DD.co_art 
	and DD.uni_principal = 1 
	and DD.co_uni = A.co_uni 
	and AA.tipo in ('V','S') 
group by AA.co_art, AA.art_des, AA.tipo, AA.tipo_imp, AA.ref, AA.co_lin, A.des_uni, DD.equivalencia, 
	ltrim(rtrim(IM.co_imag)) + '.' + LOWER(IM.co_tipo_imag)
order by AA.co_art
