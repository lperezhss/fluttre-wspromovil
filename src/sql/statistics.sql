select fec_emis datetrx,
	fact_num docnum,
	rif taxid,
	cli_des name,
	total_art total,
	unidad uom,
	co_art product_id,
	tipo type,
	(select max(cupon) from _pm_config) coupon
from _pm_vventas
where co_ven = '01'
	and year = 2015
	and month = 01
order by co_art,
	tipo,
	fact_num
