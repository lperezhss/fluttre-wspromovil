select rtrim(ltrim(xx.co_cli)) customer_id,
	rtrim(ltrim(saCliente.rif)) taxid,
	rtrim(ltrim(saCliente.co_cli)) value,
	rtrim(ltrim(saCliente.cli_des)) name,
	sum(xx.total_neto*signo) amount,
	sum(xx.saldo*signo) balance,
	(select max(cupon) from _pm_config) coupon
from (
		select *,
			case when co_tipo_doc in ('N/DB','GIRO','CHEQ','FACT','AJPA','AJPM') then 1 else -1 end signo
		from saDocumentoVenta
		where anulado = 0
			and saldo <> 0
			and #PARAMETER1#
	) xx
	join saCliente on xx.co_cli = saCliente.co_cli
group by xx.co_cli,
	saCliente.co_cli,
	saCliente.rif,
	saCliente.cli_des
order by xx.co_cli,
	saCliente.co_cli,
	saCliente.rif,
	saCliente.cli_des
