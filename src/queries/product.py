import base64

from src.utils import get_conn
from src.queries import stock


def __get_query(str_search):
    file_py = __file__
    file_sql = file_py.replace('queries', 'sql').replace('.py', '.sql')
    fd = open(file_sql, "r")
    sql_file = fd.read()
    sql_file = sql_file.replace('#PARAMETER1#', str_search)
    fd.close()

    return sql_file


def get_simple_all(str_search):
    sql = __get_query(str_search)

    conn = get_conn()
    cursor = conn.cursor()
    cursor.execute(sql)

    columns = [x[0] for x in cursor.description]  # this will extract row headers
    rows = cursor.fetchall()

    json_data = []
    for result in rows:
        json_row = dict(zip(columns, result))

        json_stock = stock.get_all(json_row['value'])
        if json_stock:
            json_row['stocks'] = json_stock
        else:
            json_row['stocks'] = False

        json_data.append(json_row)

    conn.commit()
    cursor.close()
    conn.close()

    return json_data


def get_all(str_search):
    sql = __get_query(str_search)

    conn = get_conn()
    cursor = conn.cursor()
    cursor.execute(sql)

    path_file = 'C://imagenes//'

    columns = [x[0] for x in cursor.description]  # this will extract row headers
    rows = cursor.fetchall()

    json_data = []
    for result in rows:
        json_row = dict(zip(columns, result))

        json_stock = stock.get_all(json_row['value'])
        if json_stock:
            json_row['stocks'] = json_stock

        else:
            json_row['stocks'] = False


        if json_row['file_name']:
            try:
                file_name = json_row['file_data'] = path_file + json_row['file_name']
                with open(file_name, "rb") as file_data:
                    image_string = base64.b64encode(file_data.read())
                    json_row['file_data'] = image_string.decode('utf-8')
            except Exception as ei:
                json_row['file_name'] = False
                json_row['file_data'] = False

        else:
            json_row['file_name'] = False
            json_row['file_data'] = False

        json_data.append(json_row)

    conn.commit()
    cursor.close()
    conn.close()

    return json_data
