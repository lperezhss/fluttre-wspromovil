import os
from datetime import datetime

import pymssql

from src.utils import get_conn, set_log_file


def set_order(conn, json_order):
    try:
        sql_values = "EXECUTE aa_InsertarEncabezado '#PARTNER#', '#VENDOR#', '#DATE#', '#DESCRIPTION#', '#COMMENT#', '#TERMS#'"

        sql_values = sql_values.replace('#PARTNER#', json_order['partner_code'])
        sql_values = sql_values.replace('#VENDOR#', json_order['vendor'])
        sql_values = sql_values.replace('#DATE#', json_order['date_order'])
        sql_values = sql_values.replace('#DESCRIPTION#', json_order['description'])
        sql_values = sql_values.replace('#COMMENT#', json_order['comment'])
        sql_values = sql_values.replace('#TERMS#', json_order['terms'])

        set_log_file(sql_values)

        # conn = get_conn()
        cursor = conn.cursor()

        cursor.execute(sql_values)
        rows = cursor.fetchone()
        order_id = rows[0]

        cursor.close()
        # conn.commit()
        # conn.close()

        return order_id.strip()
    except pymssql.OperationalError as e:
        raise Exception('Order: ' + str(e))
    except pymssql.InterfaceError as e:
        raise Exception('Order: ' + str(e))
    except Exception as e:
        raise Exception('Order: ' + str(e))



def set_line(conn, json_line):
    try:
        sql_values = "EXECUTE aa_InsertarRenglones '#ORDER_ID#', #LINE_NRO#, '#PRODUCT#', #QTY#, #PRICE#"

        sql_values = sql_values.replace('#ORDER_ID#', json_line['order_id'])
        sql_values = sql_values.replace('#LINE_NRO#', str(json_line['line_nro']))
        sql_values = sql_values.replace('#PRODUCT#', json_line['product'])
        sql_values = sql_values.replace('#QTY#', str(json_line['qty']))
        sql_values = sql_values.replace('#PRICE#', str(json_line['price_unit']))

        set_log_file(sql_values)

        # conn = get_conn()
        cursor = conn.cursor()
        cursor.execute(sql_values)
        cursor.close()
        # conn.close()
        # conn.commit()
    except pymssql.OperationalError as e:
        raise Exception('Line: ' + str(e))
    except pymssql.InterfaceError as e:
        raise Exception('Line: ' + str(e))
    except Exception as e:
        raise Exception('Line: ' + str(e))


def set_total(conn, order_id):
    try:
        sql_values = "EXECUTE aa_InsertarSaldo '#ORDER_ID#'"

        sql_values = sql_values.replace('#ORDER_ID#', order_id)

        set_log_file(sql_values)

        # conn = get_conn()
        cursor = conn.cursor()
        cursor.execute(sql_values)
        cursor.close()
        # conn.commit()
        # conn.close()
    except pymssql.OperationalError as e:
        raise Exception('Total: ' + str(e))
    except pymssql.InterfaceError as e:
        raise Exception('Total: ' + str(e))
    except Exception as e:
        raise Exception('Total: ' + str(e))

