from src.utils import get_conn


def __get_query(str_search):
    file_py = __file__
    file_sql = file_py.replace('queries', 'sql').replace('.py', '.sql')
    fd = open(file_sql, "r")
    sql_file = fd.read()
    if str_search != '':
        sql_file = sql_file.replace('#PARAMETER1#', "co_cli = '" + str_search + "'")
    else:
        sql_file = sql_file.replace('#PARAMETER1#', "1 = 1")
    fd.close()

    return sql_file


def get_all(str_search):
    sql = __get_query(str_search)

    conn = get_conn()
    cursor = conn.cursor()
    cursor.execute(sql)

    columns = [x[0] for x in cursor.description]  # this will extract row headers
    rows = cursor.fetchall()

    json_data = []
    for result in rows:
        json_data.append(dict(zip(columns, result)))

    conn.commit()
    cursor.close()
    conn.close()

    return json_data

def set_customer(json_partner, vendedor):
    sql_values = '''
    EXECUTE aa_InsertarCliente '#CODE#', '#NAME#', '#VENDOR#', '#ADDRESS#', '#PHONE#', '#EMAIL#'
    '''

    sql_values = sql_values.replace('#CODE#', json_partner['code'])
    sql_values = sql_values.replace('#NAME#', json_partner['name'])
    sql_values = sql_values.replace('#VENDOR#', vendedor)
    sql_values = sql_values.replace('#ADDRESS#', '')
    sql_values = sql_values.replace('#PHONE#', '')
    sql_values = sql_values.replace('#EMAIL#', '')

    conn = get_conn()
    cursor = conn.cursor()

    cursor.execute(sql_values)

    conn.commit()
    cursor.close()
    conn.close()
