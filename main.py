import flask
from flask import request, jsonify

from waitress import serve

from src.queries import cash_bank, document_summary, terms, customer, coupon, product, payment_type, category, \
    validate_user, document
from src.queries.order import set_order, set_line, set_total
from src.utils import set_log_file, get_conn

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return "<h1>Bienvenidos a wsProMovil</h1>"


@app.route('/coupon', methods=['GET'])
def coupon_all():
    items = coupon.get_all()
    return jsonify(items)


@app.route('/category', methods=['GET'])
@app.route('/category/<str_search>', methods=['GET'])
def category_all(str_search=''):
    items = category.get_all(str_search)
    return jsonify(items)


@app.route('/product', methods=['GET'])
@app.route('/product/<str_search>', methods=['GET'])
def product_all(str_search=''):
    items = product.get_all(str_search)
    return jsonify(items)


@app.route('/customer', methods=['GET'])
@app.route('/customer/<str_search>', methods=['GET'])
def customer_all(str_search=''):
    items = customer.get_all(str_search)
    return jsonify(items)


@app.route('/document', methods=['GET'])
@app.route('/document/<str_search>', methods=['GET'])
def document_all(str_search=''):
    items = document.get_all(str_search)
    return jsonify(items)


@app.route('/document_summary', methods=['GET'])
@app.route('/document_summary/<str_search>', methods=['GET'])
def document_summary_all(str_search=''):
    items = document_summary.get_all(str_search)
    return jsonify(items)


@app.route('/payment_type', methods=['GET'])
def payment_type_all():
    items = payment_type.get_all()
    return jsonify(items)


@app.route('/cash_bank', methods=['GET'])
def cash_bank_all():
    items = cash_bank.get_all()
    return jsonify(items)


@app.route('/terms', methods=['GET'])
def terms_all():
    items = terms.get_all()
    return jsonify(items)


@app.route('/validate_user/<str_search>', methods=['GET'])
def validate_user_all(str_search):
    items = validate_user.get_all(str_search)
    return jsonify(items)


@app.route('/orders', methods=['POST'])
def orders():
    json_datas = request.json.copy()

    for json_data in json_datas:
        json_order = json_data #.copy()

        if 'partner' in json_order:
            json_partner = json_order['partner'] #.copy()
            try:
                co_cli = json_partner['code']

                # del json_order['partner']
                # json_order['partner'] = co_cli
                json_order['partner_code'] = co_cli

                response_customer = customer.get_all(str_search=co_cli)
                if len(response_customer) == 0:
                    customer.set_customer(json_partner, json_data['vendor'])
            except Exception as par:
                set_log_file('Partner: ' + str(par))
                json_order['status'] = 'Failed'
                json_order['message'] = 'Partner: ' + str(par)
                continue
        else:
            json_data['status'] = 'Failed'
            json_data['message'] = 'Partner is not present in Json'
            continue

        conn = get_conn()
        try:
            try:
                order_id = set_order(conn, json_order)
            except Exception as ord:
                set_log_file(str(ord))
                raise Exception(str(ord))


            if 'lines' in json_order:
                json_lines = json_order['lines']  # .copy()
                # del json_order['lines']

                for json_line in json_lines:
                    json_line['order_id'] = order_id

                    try:
                        set_line(conn, json_line)
                    except Exception as lin:
                        set_log_file(str(lin))
                        raise Exception(str(lin))
            else:
                json_data['status'] = 'Failed'
                json_data['message'] = 'Lines is not present in Json'
                raise Exception('Lines is not present in Json')

            try:
                set_total(conn, order_id)
            except Exception as tot:
                raise Exception(str(tot))

            json_data['status'] = 'success'
            json_data['message'] = 'success'

            conn.commit()
        except Exception as ord:
            set_log_file(str(ord))
            json_data['status'] = 'failed'
            json_data['message'] = str(ord)

            conn.rollback()
            continue
        finally:
            conn.close()

    return json_datas


# if __name__ == '__main__':
#     app.run(host='0.0.0.0', port=1559)
# app.run()
serve(app, host='0.0.0.0', port=1559, threads=1)
