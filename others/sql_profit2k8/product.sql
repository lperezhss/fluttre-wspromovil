select rtrim(ltrim(art.co_art)) id,
	rtrim(ltrim(art.co_art)) value,
	rtrim(ltrim(art.art_des)) name,
	rtrim(ltrim(art.co_lin)) category_id,
	art.tipo type,
	art.tipo_imp taxtype,
	rtrim(ltrim(art.ref)) ref,
	sum(sa.stock_act - sa.stock_com) stock,
	convert(decimal(18,2),round(art.prec_vta1/1,2)) price1,
	convert(decimal(18,2),round(art.prec_vta2/1,2)) price2,
	convert(decimal(18,2),round(art.prec_vta3/1,2)) price3,
	convert(decimal(18,2),round(art.prec_vta4/1,2)) price4,
	convert(decimal(18,2),round(art.prec_vta5/1,2)) price5,
	rtrim(ltrim(una.des_uni)) uom1,
	rtrim(ltrim(unb.des_uni)) uom2,
	art.equi_uni1 equivalent1,
	art.equi_uni2 equivalent2,
	(select max(cupon) from _pm_config) coupon
from art
	join st_almac sa on art.co_art = sa.co_art
	join unidades una on art.uni_venta = una.co_uni
	join unidades unb on art.suni_venta = unb.co_uni
where art.anulado = '0'
	and (art.art_des like '%#PARAMETER1#%' or art.co_art like '%#PARAMETER1#%' or art.ref like '%#PARAMETER1#%')
group by art.co_art, art.art_des, art.prec_vta1, art.stock_act,art.stock_com,
	art.tipo, art.tipo_imp, art.ref, art.prec_vta2, art.prec_vta3, art.prec_vta4, art.prec_vta5,
	art.uni_venta, art.suni_venta, art.equi_uni1, art.equi_uni2, una.des_uni, unb.des_uni, art.co_lin
order by art.co_art