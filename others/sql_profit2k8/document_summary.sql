select xx.co_cli customer_id,
	clientes.rif taxid,
	clientes.co_cli value,
	clientes.cli_des name,
	sum(xx.monto_net*signo) amount,
	sum(xx.saldo*signo) balance,
	(select max(cupon) from _pm_config) coupon
from (
		select *,
			case when tipo_doc in ('N/DB','GIRO','CHEQ','FACT','AJPA','AJPM') then 1 else -1 end signo
		from docum_cc
		where anulado = 0
			and saldo <> 0
			and #PARAMETER1#
	) xx
	join clientes on xx.co_cli = clientes.co_cli
group by xx.co_cli,
	clientes.co_cli,
	clientes.rif,
	clientes.cli_des
order by xx.co_cli,
	clientes.co_cli,
	clientes.rif,
	clientes.cli_des