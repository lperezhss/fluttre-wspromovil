select rtrim(ltrim(co_cond)) id,
	rtrim(ltrim(co_cond)) value,
	rtrim(ltrim(cond_des)) name,
	convert(varchar(20),dias_cred) days,
	(select max(cupon) from _pm_config) coupon
from condicio
order by co_cond
