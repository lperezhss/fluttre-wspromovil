select rtrim(ltrim(BB.co_alma)) co_alma,
	sum((case when BB.tipo = 'ACT' then BB.stock else 0 end) - (case when BB.tipo = 'COM' then BB.stock else 0 end)) stock
from saStockAlmacen BB
where BB.co_art = '#PARAMETER1#'
group by  BB.co_art,
	BB.co_alma
