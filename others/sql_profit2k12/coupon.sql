SELECT empresa as name,
	cupon as coupon,
	protocolo as protocol,
	servidor as ip,
	puerto as port,
	vendedor as vendor,
	convert(varchar(20),isv) as tax,
	verstock as view_stock,
	verimpuesto as view_tax,
	convert(varchar(20),numero) as number,
	valstock as validate_stock,
	valclientessaldo as validate_balance,
	muestracondicio as view_term
FROM _pm_cupon
