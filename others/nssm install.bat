call nssm install "ProMovil Odoo" "C:\ProMovil\wsProMovil\venv\Scripts\python.exe" "C:\ProMovil\wsProMovil\main.py"
call nssm set "ProMovil Odoo" AppStdout "C:\ProMovil\wsProMovil\log\out_file.txt"
call nssm set "ProMovil Odoo" AppStderr "C:\ProMovil\wsProMovil\log\error_file.txt"
call nssm set "ProMovil Odoo" AppRotateFiles 1
call nssm set "ProMovil Odoo" AppRotateOnline 0
call nssm set "ProMovil Odoo" AppRotateSeconds 86400
call nssm set "ProMovil Odoo" AppRotateBytes 1048576
call nssm start "ProMovil Odoo"
